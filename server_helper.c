#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <errno.h>
#include <setjmp.h>
#include "np.h"
#include <pthread.h>
#include <fcntl.h>

#define NP_DEBUG

static sigjmp_buf jmpbuf;
static struct rtt_info rttinfo;
static int rttinit = 0;
static int expt_ack_no = 2;
static int sent_seq_no = 1;
static int file_index = 0;
static struct send_buf_indx global_index;
//static int recv_window_size = 12;

pthread_mutex_t index_mutex = PTHREAD_MUTEX_INITIALIZER;

void *get_in_addr(struct sockaddr *sa) {

        if (sa->sa_family == AF_INET)
                return &(((struct sockaddr_in *) sa)->sin_addr);
        else
                return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}

int fsize(FILE *in, int *size)
{
  if(fseek(in, 0, SEEK_END) == 0)
  {
    int len = ftell(in);
    if(len > 0)
    {
      if(fseek(in, 0, SEEK_SET) == 0)
      {
        *size = (size_t) len;
        return 1;
      }
    }
  }
  return 0;
}

void
sig_alrm(int signo)
{
        siglongjmp(jmpbuf, 1);
}

int dg_send_file(int sockfd, struct sockaddr *addr, int sock_size, char filename[], int wsize,int clisize) {

    	
	struct msghdr msgsend, msgrecv;
	struct iovec iovsend[2], iovrecv[1];
	struct hdr send_hdr, recv_hdr;
	pthread_t ack_thread;
	char *temp_data_ptr = NULL;
    int ack_seq_no, exp_seq_no;
    int final_segment_size;
    int adv_window_size = 12, recv_window_size = 1;
	memset(&msgsend, 0, sizeof(msgsend));
	int file_size, i=0, ret, n, check_flag;
    int pkt_transmitted, cwd = 1;
    printf("wsize %d\n", wsize);
    int ss_threshold = clisize;

    FILE *in = fopen(filename, "rb");
	if (in == NULL)
	{
		printf("\n couldn't open the file \n");
		exit(0);
	}
    int retr_count = 0, cwd_ack_count = 0;
	fsize(in, &file_size);
	char *data = (char *)malloc(file_size);
	if (fread(data, 1, file_size, in) != file_size) {
		printf(" Error in reading data into buffer \n");
		perror("File read: ");
	}
	fclose(in);

    printf("The file size is %d\n", file_size);
    int final_ack_number, end_flag=0;
    final_ack_number = file_size/SEGMENT_DATA_SIZE;
    if (file_size%SEGMENT_DATA_SIZE != 0) {
        final_ack_number += 2;
     } else {
        final_ack_number += 1;
    }
    
	temp_data_ptr = data;

	/* Initialize the timer struct if not done */
	if (rttinit == 0) {
		rtt_init(&rttinfo);
		rttinit = 1;
	}

	/* Create packet buffer */
	struct node *pkt_buffer = create_list(wsize);
	
	/* Initialize the 2 index pointers for the buffer 
	struct send_buf_indx index;  have made it global variable so that can be accessed from multiple threads
	the reason why its fine for index struct to be initialized inside the main is bcoz the other thread will
 	block at recvmsg and will check/modify value of index if ACK packet is received */

	global_index.unack_buf_ptr = pkt_buffer;
	global_index.last_sent_ptr = pkt_buffer;

	if (signal(SIGALRM, sig_alrm) == SIG_ERR) {
		perror("Signal error: ");
	}

	struct node *temp_index = global_index.last_sent_ptr;

	/* The alarm will be set here only once before the 1st segment is sent, after here its set for either when a time out happens in 
	   time out handling code or its set in the acknowledgment code where after ack is recived, the timer is cancelled for current
	   segment and reset for the next unacknowledged segment */
//    alarm(50);
    alarm(rtt_start(&rttinfo));
    printf("Time out value is %d\n", rtt_start(&rttinfo));
    rtt_newpack(&rttinfo);
    int max_transmit = 1;
    do {
        printf(" Congestion Window size %d, SS Threshold %d \n", cwd, ss_threshold);
/*****************************************************************************************************
	Sending number of packets allowed by send window and recv window constraint
*****************************************************************************************************/
        pkt_transmitted = 0;
        while ((temp_index->next != global_index.unack_buf_ptr) &&
               (adv_window_size > 0) && 
               (file_index < file_size)   && 
               (pkt_transmitted < max_transmit)) {

        /* Populate data portion of buffer */
		/* Populate header data */
        memset(&send_hdr, 0, sizeof(struct hdr));
        
        /* Handle the case of last segment */
        if ((file_index + SEGMENT_DATA_SIZE) >= file_size) {
            final_segment_size = file_size - file_index;
            memcpy(temp_index->data, temp_data_ptr, final_segment_size);
            memset(temp_index->data + final_segment_size + 1, 0, SEGMENT_DATA_SIZE - final_segment_size);
            send_hdr.fin = 1;
            send_hdr.seg_size = final_segment_size;
            temp_index->fin = 1;
            temp_index->seg_size = final_segment_size;
            printf(" The last segment number is %d \n", sent_seq_no);
        } else {
            memcpy(temp_index->data, temp_data_ptr, SEGMENT_DATA_SIZE);
            send_hdr.fin = 0;
            send_hdr.seg_size = SEGMENT_DATA_SIZE;
            temp_index->fin = 0;
            temp_index->seg_size = SEGMENT_DATA_SIZE;
        }
        
		send_hdr.seq = sent_seq_no;
		send_hdr.ack = 0;
		send_hdr.ts = rtt_ts(&rttinfo);
        memcpy(&(temp_index->header), &send_hdr, sizeof(struct hdr));

		/* populate buffer meta data */
		temp_index->ack = 0;
		temp_index->seq_no = sent_seq_no;

		
		msgsend.msg_name = NULL;
		msgsend.msg_namelen = 0;
		msgsend.msg_iov = iovsend;
		msgsend.msg_iovlen = 2;
		iovsend[0].iov_base = temp_index->data;
		iovsend[0].iov_len = SEGMENT_DATA_SIZE;
		iovsend[1].iov_base = (char *)&(temp_index->header);
		iovsend[1].iov_len = sizeof(struct hdr);

        printf(" Sending Segment No.: %d Reciever window size: %d \n", sent_seq_no, adv_window_size);
		Sendmsg(sockfd, &msgsend, 0);	
		temp_index = temp_index->next;
		temp_data_ptr += SEGMENT_DATA_SIZE;
		sent_seq_no++;
		file_index += SEGMENT_DATA_SIZE;
		global_index.last_sent_ptr = global_index.last_sent_ptr->next;
        adv_window_size--;
        pkt_transmitted++;
    }

/******************************************************************************************************
	Handle the timeout case
*******************************************************************************************************/

		
		if (sigsetjmp(jmpbuf, 1) != 0) {
                	if (rtt_timeout(&rttinfo) < 0) {
			      	printf("no response from client, giving up");
                        	rttinit = 0;    /* reinit in case we're called again */
                        	errno = ETIMEDOUT;
                        	return(-1);
                	}
                    
                    printf(" Making Congestion Window 1 due to timeout \n");
                    ss_threshold = ss_threshold/2;
                    if (ss_threshold == 0)
                        ss_threshold = 1;
                    cwd = 1;
        //            if (send_window_size/2 > 1)
          //              send_window_size = send_window_size/2;

			struct node *ind = global_index.unack_buf_ptr;
            struct hdr *resend_hdr = (struct hdr *)malloc(sizeof(struct hdr));
            memset(resend_hdr, 0, sizeof(struct hdr));
            memcpy(resend_hdr, &(ind->header), sizeof(struct hdr));
            printf(" Retransmitting seq no. %d \n", resend_hdr->seq);
			(ind->header).ts = rtt_ts(&rttinfo);
			msgsend.msg_name = NULL;
        		msgsend.msg_namelen = 0;
        		msgsend.msg_iov = iovsend;
        		msgsend.msg_iovlen = 2;
        		iovsend[0].iov_base = ind->data;
        		iovsend[0].iov_len = SEGMENT_DATA_SIZE;
        		iovsend[1].iov_base = (char *)resend_hdr;
        		iovsend[1].iov_len = sizeof(struct hdr);
                printf(" Resending Packet Seq No.: %d \n", ind->header.seq);
			Sendmsg(sockfd, &msgsend, 0);
            alarm(1);
            //			alarm(rtt_start(&rttinfo));
       	}

    /*    Handle Acknowledgments */
	memset(&msgrecv, 0, sizeof(struct msghdr));

	msgrecv.msg_name = NULL; 
	msgrecv.msg_namelen = 0;
	msgrecv.msg_iov = iovrecv;
	msgrecv.msg_iovlen = 1;
	iovrecv[0].iov_base = (char *)&recv_hdr;
	iovrecv[0].iov_len = sizeof(struct hdr);
    struct node *temp;
    Recvmsg(sockfd, &msgrecv, 0);
        check_flag = 0;
        if (recv_hdr.ack == 1) {
         
            if (cwd <= ss_threshold) {
                max_transmit = 2;
               cwd++;
            } else {
                cwd_ack_count++;
                max_transmit = 1;
                if (cwd_ack_count%cwd == 0) {
                    max_transmit = 2;
                    cwd++;
                    cwd_ack_count = 0;
                }
   //             max_transmit = 
            }

/*            if (recv_hdr.seq == 2) {
               recv_window_size = recv_hdr.win_size + 1; 
            } */

            adv_window_size = recv_hdr.win_size;
			ack_seq_no = recv_hdr.seq-1;  // the ACK number in the received ACK packet
			exp_seq_no = global_index.unack_buf_ptr->seq_no; // seq_no of the packet pointed by unack_buf_ptr, unack_buf_ptr points
								  // to the oldest unacked segment
	        printf(" Segments received till %d \n", ack_seq_no);
            if (ack_seq_no == final_ack_number-1) {
                printf(" Final ACK received \n");
                end_flag = 1;
            }

/*            if (send_window_size < recv_window_size) {
                send_window_size++;
                printf("Increasing send window size by 1\n");
            } */

            if (ack_seq_no == exp_seq_no) {  // the ACK pkt received is expected one
				alarm(0);
				global_index.unack_buf_ptr = global_index.unack_buf_ptr->next;
                global_index.unack_buf_ptr->ack_count = 1;
                rtt_packwatch(&rttinfo);
                alarm(1);
      //          alarm(rtt_start(&rttinfo));
			}
            /* The received ACK is for packet later than expected, this means that all the packet till
               the one acknowledged by the ACK packet has been received the receiver */
            else if (ack_seq_no > exp_seq_no) {
                int temp_seq_no = exp_seq_no;
                temp = global_index.unack_buf_ptr;
               
                while(temp_seq_no != ack_seq_no) {
                    temp = temp->next;
                    temp_seq_no++;
                    if (temp == global_index.last_sent_ptr) 
                        check_flag = 1;
                }

                if (check_flag != 1) {
                    alarm(0);
                    rtt_packwatch(&rttinfo);
                    global_index.unack_buf_ptr = temp->next;
                    global_index.unack_buf_ptr->ack_count = 1;
                    alarm(1);
                } else {

                    printf(" ACK received for packet not sent \n");
                }
			} else {
                    global_index.unack_buf_ptr->ack_count++;
                    if (global_index.unack_buf_ptr->ack_count == 4) {
                        alarm(0);
                        ss_threshold = ss_threshold/2;
                        if (ss_threshold == 0) 
                            ss_threshold = 1;
                        cwd = cwd/2; 
			if (cwd == 0) 
                            cwd = 1;
            			struct node *ind = global_index.unack_buf_ptr;
                        struct hdr *resend_hdr = (struct hdr *)malloc(sizeof(struct hdr));
                        memset(resend_hdr, 0, sizeof(struct hdr));
                        memcpy(resend_hdr, &(ind->header), sizeof(struct hdr));
            			(ind->header).ts = rtt_ts(&rttinfo);
            			msgsend.msg_name = NULL;
                		msgsend.msg_namelen = 0;
                 		msgsend.msg_iov = iovsend;
                   		msgsend.msg_iovlen = 2;
                 		iovsend[0].iov_base = ind->data;
             		    iovsend[0].iov_len = SEGMENT_DATA_SIZE;
             	    	iovsend[1].iov_base = (char *)resend_hdr;
                 		iovsend[1].iov_len = sizeof(struct hdr);
                        printf(" Resending Packet Seq No.: %d \n", ind->header.seq);
             			Sendmsg(sockfd, &msgsend, 0);
                        alarm(1);
                    //    global_index.unack_buf_ptr->ack_count = 0;
                    }
				// its duplicate ACK, do nothing just discard the segment
			}					
		}
    } while (end_flag == 0);
}


