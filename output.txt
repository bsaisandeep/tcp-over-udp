#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <errno.h>
#include <pthread.h>
#include <math.h>
#include <setjmp.h>
#include "np.h"
#include "np1.h"

static int expt_seq_no = 1;
//static int window_size = 12;
struct recv_buf_indx global_index;
static sigjmp_buf jmpbuf;

pthread_mutex_t index_mutex = PTHREAD_MUTEX_INITIALIZER;



/*
int main()
{
	struct sockaddr_in servaddr, remaddr;
        char line1[MAXLINE], recvline[MAXLINE], buf[MAXLINE], sendline[MAXLINE];
        int sockfd, n;

	// Creating address structure for self 
        bzero(&servaddr, sizeof(servaddr));
        servaddr.sin_family = AF_INET;
        servaddr.sin_port = htons(3491);
        inet_pton(AF_INET, "127.0.0.1", &servaddr.sin_addr);

	// Creating address structure for remote end 
	bzero(&remaddr, sizeof(remaddr));
	remaddr.sin_family = AF_INET;
	remaddr.sin_port = htons(3490);
	inet_pton(AF_INET, "127.0.0.1", &remaddr.sin_addr);

	// Create local socket and bind it to local address and port no. 3491 
        sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	bind(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr));

	// Connect the local socket to remote address 
	if (connect(sockfd, (struct sockaddr *)&remaddr, sizeof(remaddr)) < 0) {
		perror("Connection error\n");
	}

	dg_recv(sockfd, (struct sockaddr *)&remaddr, sizeof(remaddr));	
//	dg_cli(stdin, sockfd, &servaddr, sizeof(servaddr)); 

	return 1;
}
*/
void sig_alrm(int signo) 
{
    siglongjmp(jmpbuf, 1);
}

int dg_recv(int sockfd, struct sockaddr *remaddr1, int sock_len, struct clientinputs inputs)
{
    printf("Size of header is %d\n", sizeof(struct hdr));
	printf("In function %s\n", __func__);
	struct msghdr msgrecv, msgsend;
	struct sockaddr_in remaddr;
	struct iovec iovrecv[2], iovsend[1];
	struct hdr recv_hdr, send_hdr;
	pthread_t reader_thread;
	char *pack_buf = (char *)malloc(SEGMENT_DATA_SIZE);
	int n, final_seq_no = -1;
	memset(&msgrecv, 0, sizeof(msgrecv));
	remaddr.sin_family = AF_INET;	

    /* Set signal handler for SIGALRM */
    if (signal(SIGALRM, sig_alrm) == SIG_ERR) {
        perror("Signal Error: ");
    }    
	/* Create packet buffer */
	struct node *pkt_buffer = create_list(inputs.cliwinsize);

	global_index.expt_seg_ptr = pkt_buffer;
	global_index.unread_buf_ptr = pkt_buffer;

	/* 
	   Receive the packet and check for its seq no 
	   seq_no > expt_seq_no (traverse to buffer which will store this packet, and set recv_flag)
	   seq_no < expt_seq_no (its a duplicate packet, form a seq_no of the packet, form a ACK packet with this seq_no and send it out)
	   seq_no == expt_seq_no (copy the data part into buffer, increment expt_seq_no, send out ACK packet with this expt_seq_no)
	*/
    struct thread_param params;
    params.time_val = inputs.timeinmsec;
	pthread_create(&reader_thread, NULL, reader, (void *)&params);
    pthread_detach(&reader_thread);	

	msgrecv.msg_name = NULL;
        msgrecv.msg_namelen = 0;
        msgrecv.msg_iov = iovrecv;
        msgrecv.msg_iovlen = 2;
        iovrecv[0].iov_base = pack_buf;
        iovrecv[0].iov_len = SEGMENT_DATA_SIZE;
        iovrecv[1].iov_base = &recv_hdr;
        iovrecv[1].iov_len = sizeof(struct hdr);
        srand48(inputs.seedval);

	while((n = recvmsg(sockfd, &msgrecv, 0)) > 0) {
        alarm(0); // this is needed in the last ACK sent to the server is lost and it needs to be retransmitted
        /* Compare seq no. of received packet with expt_seq_no, and apply the rules mentioned above */
		if (drand48() < inputs.probval) {
            printf("Dropping segment no.: %d\n", recv_hdr.seq);
			goto drop_packet;
		}  

        if (global_index.expt_seg_ptr->next == global_index.unread_buf_ptr) {
            printf("Receive buffer full, dropping packet\n");
            goto drop_packet;
        }        

        if (recv_hdr.fin == 1) {
            final_seq_no = recv_hdr.seq;
            printf("Final sequence number is %d\n", final_seq_no);
            printf("********************************************\n");
            printf("********************************************\n");
            printf("********************************************\n");
            printf("********************************************\n");
            printf("********************************************\n");
            printf("********************************************\n");
            printf("********************************************\n");
            printf("********************************************\n");
            printf("********************************************\n");
            printf("********************************************\n");
            printf("********************************************\n");
            printf("********************************************\n");
        }
/*        char  *dump = (char *)&msgrecv;
        int i;
        printf("\n");
        for (i=0;i<sizeof(struct msghdr);i++) {
            printf("%02x\t", *(dump+i));
        }
        printf("\n");
  */      int flag = 0;
		struct node *temp_index;
        printf("Received Packet Seq No.: %d\n", recv_hdr.seq);
        if (recv_hdr.seq > expt_seq_no) {
			int temp_seq = expt_seq_no;
            temp_index = global_index.expt_seg_ptr;
			while (temp_seq != recv_hdr.seq) {
				temp_index = temp_index->next;
				temp_seq++;
                if (temp_index == global_index.unread_buf_ptr)
                    flag = 1;
			}

            if (flag != 1) {
                temp_index->fin = recv_hdr.fin;
                temp_index->seg_size = recv_hdr.seg_size;
		    	memcpy(temp_index->data, pack_buf, recv_hdr.seg_size);
    			memcpy(&(temp_index->header), &recv_hdr, sizeof(struct hdr));
    			temp_index->recv_flag = 1;
            } else {
                printf("Dropping Segmet No. %d out of bound\n", recv_hdr.seq);
            }
            goto ack_send;
		} else if (recv_hdr.seq < expt_seq_no) {
            goto ack_send;
		} else if (recv_hdr.seq == expt_seq_no) {
		
            pthread_mutex_lock(&index_mutex);
            temp_index = global_index.expt_seg_ptr;
            temp_index->fin = recv_hdr.fin;
            temp_index->seg_size = recv_hdr.seg_size;
			memcpy(temp_index->data, pack_buf, recv_hdr.seg_size);
			memcpy(&(temp_index->header), &recv_hdr, sizeof(struct hdr));

			temp_index = temp_index->next;
			expt_seq_no++;
			while(temp_index->recv_flag == 1) {
				temp_index->recv_flag = 0;
				expt_seq_no++;
				temp_index = temp_index->next;
			}			
			global_index.expt_seg_ptr = temp_index;
            pthread_mutex_unlock(&index_mutex);
            goto ack_send;
		}

    ack_send:
    printf("ACK: %d     Advertised Window Size:  %d\n", expt_seq_no, window_size(inputs.cliwinsize));
    memset(&msgsend, 0, sizeof(struct msghdr));
    memcpy(&send_hdr, &recv_hdr, sizeof(struct hdr));
    send_hdr.seq = expt_seq_no;
    send_hdr.ack = 1;
    send_hdr.win_size = window_size(inputs.cliwinsize);

    msgsend.msg_name = NULL;
    msgsend.msg_namelen = 0;
    msgsend.msg_iov = iovsend;
    msgsend.msg_iovlen = 1;
    iovsend[0].iov_base = (char *)&send_hdr;
    iovsend[0].iov_len = sizeof(struct hdr);
    /* Handle separately the case when last ACK is being sent */
    if ((final_seq_no > 0) && (expt_seq_no == final_seq_no+1)) {
        printf("Sending final ACk\n");
        alarm(2);
		if (drand48() < inputs.probval) {
            printf("Dropping ACK no.: %d\n", expt_seq_no);
			goto drop_packet;
		}  
        if (sendmsg(sockfd, &msgsend, 0) < 0) {
            printf("Error in sending ACK\n");
            perror("ACK: ");
        }
    } else {
		if (drand48() < inputs.probval) {
            printf("Dropping ACK no.: %d\n", expt_seq_no);
			goto drop_packet;
		}  
        if (sendmsg(sockfd, &msgsend, 0) < 0) {
            printf("Error in sending ACK\n");
            perror("ACK: ");
        }
    }

    drop_packet:
    printf("");	
    if (sigsetjmp(jmpbuf, 1) != 0) {
        printf("Last ACK received by sender, shutting down connection\n");
        close(sockfd);
        return;
    }

    }

	close(sockfd);
}
    
/* Add logic for calculating the window size, and updating it in the ack packet 
int send_ack(int sockfd, struct hdr *header) {

	struct hdr send_hdr;
	struct msghdr msgsend;
	struct iovec iovsend[1];

    printf("ACK: %d     Advertised Window Size:  %d\n", expt_seq_no, window_size());    
    memset(&msgsend, 0, sizeof(struct msghdr));
	memcpy(&send_hdr, header, sizeof(struct hdr));
	send_hdr.seq = expt_seq_no;
	send_hdr.ack = 1;
	send_hdr.win_size = window_size();

    // Final ACK is being sent, handle the time out in case the ACK is lost 
    if (send_hdr.fin == 1) {


    }
    	msgsend.msg_name = NULL;
        msgsend.msg_namelen = 0;
        msgsend.msg_iov = iovsend;
        msgsend.msg_iovlen = 1;
        iovsend[0].iov_base = (char *)&send_hdr;
        iovsend[0].iov_len = sizeof(struct hdr);	

    if (sendmsg(sockfd, &msgsend, 0) < 0) {
		printf("error in sending ack\n");
		perror("ACK:  ");
	}
}
*/
/* Reader thread will read data from the buffer and update the index, need to add mutex code */
void *reader(void *arg)
{
    struct node *temp;
    int time_val = ((struct thread_param *)arg)->time_val, sleep_value;
    struct timeval tv;
    FILE *fp = fopen("output.txt", "w");
    while (1) {
    pthread_mutex_lock(&index_mutex);
	temp = global_index.unread_buf_ptr;
	/* need to take into account the case of last packet when the data read will be less than 300 */
	char *buffer = (char *)malloc(SEGMENT_DATA_SIZE+1); 
//    fp = fopen("output.txt", "a");
	while(temp != global_index.expt_seg_ptr) {
		memcpy(buffer, temp->data, temp->seg_size);
		buffer[temp->seg_size+1] = '\0';
		printf("%s\n", buffer);
        fwrite(buffer, 1, temp->seg_size, fp);
        temp = temp->next;	

     //   printf("Moving unread_buf_ptr\n");
	}
//    fclose(fp);
	global_index.unread_buf_ptr = temp;
    pthread_mutex_unlock(&index_mutex);
    if (gettimeofday(&tv, NULL) == -1) {
        perror("");
    }
    sleep_value = (-1)*time_val*log(random_gen(tv.tv_sec + tv.tv_usec));
    sleep_value *= 1000;
    usleep(sleep_value);
    }
    fclose(fp);
}

int window_size(int size)
{
	
	struct node *temp_index = global_index.expt_seg_ptr;
	int temp_window_size = 0;

    if (temp_index == global_index.unread_buf_ptr) {
        return size;
    }

	while(temp_index != global_index.unread_buf_ptr) {
		temp_index = temp_index->next;
		temp_window_size++;
	}
	
	return temp_window_size;
}
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <errno.h>
#include <setjmp.h>
#include "np.h"
#include <pthread.h>
#include <fcntl.h>

#define NP_DEBUG

static sigjmp_buf jmpbuf;
static struct rtt_info rttinfo;
static int rttinit = 0;
static int expt_ack_no = 2;
static int sent_seq_no = 1;
static int file_index = 0;
static struct send_buf_indx global_index;
//static int recv_window_size = 12;

pthread_mutex_t index_mutex = PTHREAD_MUTEX_INITIALIZER;

void *get_in_addr(struct sockaddr *sa) {

        if (sa->sa_family == AF_INET)
                return &(((struct sockaddr_in *) sa)->sin_addr);
        else
                return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}

int fsize(FILE *in, int *size)
{
  if(fseek(in, 0, SEEK_END) == 0)
  {
    int len = ftell(in);
    if(len > 0)
    {
      if(fseek(in, 0, SEEK_SET) == 0)
      {
        *size = (size_t) len;
        return 1;
      }
    }
  }
  return 0;
}

void
sig_alrm(int signo)
{
        siglongjmp(jmpbuf, 1);
}

int dg_send_file(int sockfd, struct sockaddr *addr, int sock_size, char filename[], int wsize,int clisize) {

    	
	struct msghdr msgsend, msgrecv;
	struct iovec iovsend[2], iovrecv[1];
	struct hdr send_hdr, recv_hdr;
	pthread_t ack_thread;
	char *temp_data_ptr = NULL;
    int ack_seq_no, exp_seq_no;
    int final_segment_size;
    int adv_window_size = 12, recv_window_size = 1;
	memset(&msgsend, 0, sizeof(msgsend));
	int file_size, i=0, ret, n, check_flag;
    int pkt_transmitted, cwd = 1;
    printf("wsize %d\n", wsize);
    int ss_threshold = clisize;

    FILE *in = fopen(filename, "rb");
	if (in == NULL)
	{
		printf("\n couldn't open the file \n");
		exit(0);
	}
    int retr_count = 0, cwd_ack_count = 0;
	fsize(in, &file_size);
	char *data = (char *)malloc(file_size);
	if (fread(data, 1, file_size, in) != file_size) {
		printf(" Error in reading data into buffer \n");
		perror("File read: ");
	}
	fclose(in);

    printf("The file size is %d\n", file_size);
    int final_ack_number, end_flag=0;
    final_ack_number = file_size/SEGMENT_DATA_SIZE;
    if (file_size%SEGMENT_DATA_SIZE != 0) {
        final_ack_number += 2;
     } else {
        final_ack_number += 1;
    }
    
	temp_data_ptr = data;

	/* Initialize the timer struct if not done */
	if (rttinit == 0) {
		rtt_init(&rttinfo);
		rttinit = 1;
	}

	/* Create packet buffer */
	struct node *pkt_buffer = create_list(wsize);
	
	/* Initialize the 2 index pointers for the buffer 
	struct send_buf_indx index;  have made it global variable so that can be accessed from multiple threads
	the reason why its fine for index struct to be initialized inside the main is bcoz the other thread will
 	block at recvmsg and will check/modify value of index if ACK packet is received */

	global_index.unack_buf_ptr = pkt_buffer;
	global_index.last_sent_ptr = pkt_buffer;

	if (signal(SIGALRM, sig_alrm) == SIG_ERR) {
		perror("Signal error: ");
	}

	struct node *temp_index = global_index.last_sent_ptr;

	/* The alarm will be set here only once before the 1st segment is sent, after here its set for either when a time out happens in 
	   time out handling code or its set in the acknowledgment code where after ack is recived, the timer is cancelled for current
	   segment and reset for the next unacknowledged segment */
//    alarm(50);
    alarm(rtt_start(&rttinfo));
    printf("Time out value is %d\n", rtt_start(&rttinfo));
    rtt_newpack(&rttinfo);
    int max_transmit = 1;
    do {
        printf(" Congestion Window size %d, SS Threshold %d \n", cwd, ss_threshold);
/*****************************************************************************************************
	Sending number of packets allowed by send window and recv window constraint
*****************************************************************************************************/
        pkt_transmitted = 0;
        while ((temp_index->next != global_index.unack_buf_ptr) &&
               (adv_window_size > 0) && 
               (file_index < file_size)   && 
               (pkt_transmitted < max_transmit)) {

        /* Populate data portion of buffer */
		/* Populate header data */
        memset(&send_hdr, 0, sizeof(struct hdr));
        
        /* Handle the case of last segment */
        if ((file_index + SEGMENT_DATA_SIZE) >= file_size) {
            final_segment_size = file_size - file_index;
            memcpy(temp_index->data, temp_data_ptr, final_segment_size);
            memset(temp_index->data + final_segment_size + 1, 0, SEGMENT_DATA_SIZE - final_segment_size);
            send_hdr.fin = 1;
            send_hdr.seg_size = final_segment_size;
            temp_index->fin = 1;
            temp_index->seg_size = final_segment_size;
            printf(" The last segment number is %d \n", sent_seq_no);
        } else {
            memcpy(temp_index->data, temp_data_ptr, SEGMENT_DATA_SIZE);
            send_hdr.fin = 0;
            send_hdr.seg_size = SEGMENT_DATA_SIZE;
            temp_index->fin = 0;
            temp_index->seg_size = SEGMENT_DATA_SIZE;
        }
        
		send_hdr.seq = sent_seq_no;
		send_hdr.ack = 0;
		send_hdr.ts = rtt_ts(&rttinfo);
        memcpy(&(temp_index->header), &send_hdr, sizeof(struct hdr));

		/* populate buffer meta data */
		temp_index->ack = 0;
		temp_index->seq_no = sent_seq_no;

		
		msgsend.msg_name = NULL;
		msgsend.msg_namelen = 0;
		msgsend.msg_iov = iovsend;
		msgsend.msg_iovlen = 2;
		iovsend[0].iov_base = temp_index->data;
		iovsend[0].iov_len = SEGMENT_DATA_SIZE;
		iovsend[1].iov_base = (char *)&(temp_index->header);
		iovsend[1].iov_len = sizeof(struct hdr);

        printf(" Sending Segment No.: %d Reciever window size: %d \n", sent_seq_no, adv_window_size);
		Sendmsg(sockfd, &msgsend, 0);	
		temp_index = temp_index->next;
		temp_data_ptr += SEGMENT_DATA_SIZE;
		sent_seq_no++;
		file_index += SEGMENT_DATA_SIZE;
		global_index.last_sent_ptr = global_index.last_sent_ptr->next;
        adv_window_size--;
        pkt_transmitted++;
    }

/******************************************************************************************************
	Handle the timeout case
*******************************************************************************************************/

		
		if (sigsetjmp(jmpbuf, 1) != 0) {
                	if (rtt_timeout(&rttinfo) < 0) {
			      	printf("no response from client, giving up");
                        	rttinit = 0;    /* reinit in case we're called again */
                        	errno = ETIMEDOUT;
                        	return(-1);
                	}
                    
                    printf(" Making Congestion Window 1 due to timeout \n");
                    ss_threshold = ss_threshold/2;
                    if (ss_threshold == 0)
                        ss_threshold = 1;
                    cwd = 1;
        //            if (send_window_size/2 > 1)
          //              send_window_size = send_window_size/2;

			struct node *ind = global_index.unack_buf_ptr;
            struct hdr *resend_hdr = (struct hdr *)malloc(sizeof(struct hdr));
            memset(resend_hdr, 0, sizeof(struct hdr));
            memcpy(resend_hdr, &(ind->header), sizeof(struct hdr));
            printf(" Retransmitting seq no. %d \n", resend_hdr->seq);
			(ind->header).ts = rtt_ts(&rttinfo);
			msgsend.msg_name = NULL;
        		msgsend.msg_namelen = 0;
        		msgsend.msg_iov = iovsend;
        		msgsend.msg_iovlen = 2;
        		iovsend[0].iov_base = ind->data;
        		iovsend[0].iov_len = SEGMENT_DATA_SIZE;
        		iovsend[1].iov_base = (char *)resend_hdr;
        		iovsend[1].iov_len = sizeof(struct hdr);
                printf(" Resending Packet Seq No.: %d \n", ind->header.seq);
			Sendmsg(sockfd, &msgsend, 0);
            alarm(1);
            //			alarm(rtt_start(&rttinfo));
       	}

    /*    Handle Acknowledgments */
	memset(&msgrecv, 0, sizeof(struct msghdr));

	msgrecv.msg_name = NULL; 
	msgrecv.msg_namelen = 0;
	msgrecv.msg_iov = iovrecv;
	msgrecv.msg_iovlen = 1;
	iovrecv[0].iov_base = (char *)&recv_hdr;
	iovrecv[0].iov_len = sizeof(struct hdr);
    struct node *temp;
    Recvmsg(sockfd, &msgrecv, 0);
        check_flag = 0;
        if (recv_hdr.ack == 1) {
         
            if (cwd <= ss_threshold) {
                max_transmit = 2;
               cwd++;
            } else {
                cwd_ack_count++;
                max_transmit = 1;
                if (cwd_ack_count%cwd == 0) {
                    max_transmit = 2;
                    cwd++;
                    cwd_ack_count = 0;
                }
   //             max_transmit = 
            }

/*            if (recv_hdr.seq == 2) {
               recv_window_size = recv_hdr.win_size + 1; 
            } */

            adv_window_size = recv_hdr.win_size;
			ack_seq_no = recv_hdr.seq-1;  // the ACK number in the received ACK packet
			exp_seq_no = global_index.unack_buf_ptr->seq_no; // seq_no of the packet pointed by unack_buf_ptr, unack_buf_ptr points
								  // to the oldest unacked segment
	        printf(" Segments received till %d \n", ack_seq_no);
            if (ack_seq_no == final_ack_number-1) {
                printf(" Final ACK received \n");
                end_flag = 1;
            }

/*            if (send_window_size < recv_window_size) {
                send_window_size++;
                printf("Increasing send window size by 1\n");
            } */

            if (ack_seq_no == exp_seq_no) {  // the ACK pkt received is expected one
				alarm(0);
				global_index.unack_buf_ptr = global_index.unack_buf_ptr->next;
                global_index.unack_buf_ptr->ack_count = 1;
                rtt_packwatch(&rttinfo);
                alarm(1);
      //          alarm(rtt_start(&rttinfo));
			}
            /* The received ACK is for packet later than expected, this means that all the packet till
               the one acknowledged by the ACK packet has been received the receiver */
            else if (ack_seq_no > exp_seq_no) {
                int temp_seq_no = exp_seq_no;
                temp = global_index.unack_buf_ptr;
               
                while(temp_seq_no != ack_seq_no) {
                    temp = temp->next;
                    temp_seq_no++;
                    if (temp == global_index.last_sent_ptr) 
                        check_flag = 1;
                }

                if (check_flag != 1) {
                    alarm(0);
                    rtt_packwatch(&rttinfo);
                    global_index.unack_buf_ptr = temp->next;
                    global_index.unack_buf_ptr->ack_count = 1;
                    alarm(1);
                } else {

                    printf(" ACK received for packet not sent \n");
                }
			} else {
                    global_index.unack_buf_ptr->ack_count++;
                    if (global_index.unack_buf_ptr->ack_count == 4) {
                        alarm(0);
                        ss_threshold = ss_threshold/2;
                        if (ss_threshold == 0) 
                            ss_threshold = 1;
                        cwd = cwd/2; 
			if (cwd == 0) 
                            cwd = 1;
            			struct node *ind = global_index.unack_buf_ptr;
                        struct hdr *resend_hdr = (struct hdr *)malloc(sizeof(struct hdr));
                        memset(resend_hdr, 0, sizeof(struct hdr));
                        memcpy(resend_hdr, &(ind->header), sizeof(struct hdr));
            			(ind->header).ts = rtt_ts(&rttinfo);
            			msgsend.msg_name = NULL;
                		msgsend.msg_namelen = 0;
                 		msgsend.msg_iov = iovsend;
                   		msgsend.msg_iovlen = 2;
                 		iovsend[0].iov_base = ind->data;
             		    iovsend[0].iov_len = SEGMENT_DATA_SIZE;
             	    	iovsend[1].iov_base = (char *)resend_hdr;
                 		iovsend[1].iov_len = sizeof(struct hdr);
                        printf(" Resending Packet Seq No.: %d \n", ind->header.seq);
             			Sendmsg(sockfd, &msgsend, 0);
                        alarm(1);
                    //    global_index.unack_buf_ptr->ack_count = 0;
                    }
				// its duplicate ACK, do nothing just discard the segment
			}					
		}
    } while (end_flag == 0);
}


