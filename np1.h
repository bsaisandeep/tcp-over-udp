struct clientinputs{
	in_addr_t servipaddress;
	int servport;
	char filename[256];	
	int cliwinsize;
	int seedval;
	double probval;
	int timeinmsec;
};

int dg_send_file(int sockfd, struct sockaddr *pcliaddr, socklen_t clilen, char filename[],int wsize,int clisize);
int dg_recv(int sockfd, struct sockaddr *remaddr1, int sock_len, struct clientinputs inputs);

