#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <errno.h>
#include <pthread.h>
#include <math.h>
#include <setjmp.h>
#include "np.h"
#include "np1.h"

static int expt_seq_no = 1;
//static int window_size = 12;
struct recv_buf_indx global_index;
static sigjmp_buf jmpbuf;

pthread_mutex_t index_mutex = PTHREAD_MUTEX_INITIALIZER;



/*
int main()
{
	struct sockaddr_in servaddr, remaddr;
        char line1[MAXLINE], recvline[MAXLINE], buf[MAXLINE], sendline[MAXLINE];
        int sockfd, n;

	// Creating address structure for self 
        bzero(&servaddr, sizeof(servaddr));
        servaddr.sin_family = AF_INET;
        servaddr.sin_port = htons(3491);
        inet_pton(AF_INET, "127.0.0.1", &servaddr.sin_addr);

	// Creating address structure for remote end 
	bzero(&remaddr, sizeof(remaddr));
	remaddr.sin_family = AF_INET;
	remaddr.sin_port = htons(3490);
	inet_pton(AF_INET, "127.0.0.1", &remaddr.sin_addr);

	// Create local socket and bind it to local address and port no. 3491 
        sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	bind(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr));

	// Connect the local socket to remote address 
	if (connect(sockfd, (struct sockaddr *)&remaddr, sizeof(remaddr)) < 0) {
		perror("Connection error\n");
	}

	dg_recv(sockfd, (struct sockaddr *)&remaddr, sizeof(remaddr));	
//	dg_cli(stdin, sockfd, &servaddr, sizeof(servaddr)); 

	return 1;
}
*/
void sig_alrm(int signo) 
{
    siglongjmp(jmpbuf, 1);
}

int dg_recv(int sockfd, struct sockaddr *remaddr1, int sock_len, struct clientinputs inputs)
{
    printf("Size of header is %d\n", sizeof(struct hdr));
	printf("In function %s\n", __func__);
	struct msghdr msgrecv, msgsend;
	struct sockaddr_in remaddr;
	struct iovec iovrecv[2], iovsend[1];
	struct hdr recv_hdr, send_hdr;
	pthread_t reader_thread;
	char *pack_buf = (char *)malloc(SEGMENT_DATA_SIZE);
	int n, final_seq_no = -1;
	memset(&msgrecv, 0, sizeof(msgrecv));
	remaddr.sin_family = AF_INET;	

    /* Set signal handler for SIGALRM */
    if (signal(SIGALRM, sig_alrm) == SIG_ERR) {
        perror("Signal Error: ");
    }    
	/* Create packet buffer */
	struct node *pkt_buffer = create_list(inputs.cliwinsize);

	global_index.expt_seg_ptr = pkt_buffer;
	global_index.unread_buf_ptr = pkt_buffer;

	/* 
	   Receive the packet and check for its seq no 
	   seq_no > expt_seq_no (traverse to buffer which will store this packet, and set recv_flag)
	   seq_no < expt_seq_no (its a duplicate packet, form a seq_no of the packet, form a ACK packet with this seq_no and send it out)
	   seq_no == expt_seq_no (copy the data part into buffer, increment expt_seq_no, send out ACK packet with this expt_seq_no)
	*/
    struct thread_param params;
    params.time_val = inputs.timeinmsec;
	pthread_create(&reader_thread, NULL, reader, (void *)&params);
    pthread_detach(&reader_thread);	

	msgrecv.msg_name = NULL;
        msgrecv.msg_namelen = 0;
        msgrecv.msg_iov = iovrecv;
        msgrecv.msg_iovlen = 2;
        iovrecv[0].iov_base = pack_buf;
        iovrecv[0].iov_len = SEGMENT_DATA_SIZE;
        iovrecv[1].iov_base = &recv_hdr;
        iovrecv[1].iov_len = sizeof(struct hdr);
        srand48(inputs.seedval);

	while((n = recvmsg(sockfd, &msgrecv, 0)) > 0) {
        alarm(0); // this is needed in the last ACK sent to the server is lost and it needs to be retransmitted
        /* Compare seq no. of received packet with expt_seq_no, and apply the rules mentioned above */
		if (drand48() < inputs.probval) {
            printf("Dropping segment no.: %d\n", recv_hdr.seq);
			goto drop_packet;
		}  

        if (global_index.expt_seg_ptr->next == global_index.unread_buf_ptr) {
            printf("Receive buffer full, dropping packet\n");
            goto drop_packet;
        }        

        if (recv_hdr.fin == 1) {
            final_seq_no = recv_hdr.seq;
            printf("Final sequence number is %d\n", final_seq_no);
            printf("********************************************\n");
            printf("********************************************\n");
            printf("********************************************\n");
            printf("********************************************\n");
            printf("********************************************\n");
            printf("********************************************\n");
            printf("********************************************\n");
            printf("********************************************\n");
            printf("********************************************\n");
            printf("********************************************\n");
            printf("********************************************\n");
            printf("********************************************\n");
        }
/*        char  *dump = (char *)&msgrecv;
        int i;
        printf("\n");
        for (i=0;i<sizeof(struct msghdr);i++) {
            printf("%02x\t", *(dump+i));
        }
        printf("\n");
  */      int flag = 0;
		struct node *temp_index;
        printf("Received Packet Seq No.: %d\n", recv_hdr.seq);
        if (recv_hdr.seq > expt_seq_no) {
			int temp_seq = expt_seq_no;
            temp_index = global_index.expt_seg_ptr;
			while (temp_seq != recv_hdr.seq) {
				temp_index = temp_index->next;
				temp_seq++;
                if (temp_index == global_index.unread_buf_ptr)
                    flag = 1;
			}

            if (flag != 1) {
                temp_index->fin = recv_hdr.fin;
                temp_index->seg_size = recv_hdr.seg_size;
		    	memcpy(temp_index->data, pack_buf, recv_hdr.seg_size);
    			memcpy(&(temp_index->header), &recv_hdr, sizeof(struct hdr));
    			temp_index->recv_flag = 1;
            } else {
                printf("Dropping Segmet No. %d out of bound\n", recv_hdr.seq);
            }
            goto ack_send;
		} else if (recv_hdr.seq < expt_seq_no) {
            goto ack_send;
		} else if (recv_hdr.seq == expt_seq_no) {
		
            pthread_mutex_lock(&index_mutex);
            temp_index = global_index.expt_seg_ptr;
            temp_index->fin = recv_hdr.fin;
            temp_index->seg_size = recv_hdr.seg_size;
			memcpy(temp_index->data, pack_buf, recv_hdr.seg_size);
			memcpy(&(temp_index->header), &recv_hdr, sizeof(struct hdr));

			temp_index = temp_index->next;
			expt_seq_no++;
			while(temp_index->recv_flag == 1) {
				temp_index->recv_flag = 0;
				expt_seq_no++;
				temp_index = temp_index->next;
			}			
			global_index.expt_seg_ptr = temp_index;
            pthread_mutex_unlock(&index_mutex);
            goto ack_send;
		}

    ack_send:
    printf("ACK: %d     Advertised Window Size:  %d\n", expt_seq_no, window_size(inputs.cliwinsize));
    memset(&msgsend, 0, sizeof(struct msghdr));
    memcpy(&send_hdr, &recv_hdr, sizeof(struct hdr));
    send_hdr.seq = expt_seq_no;
    send_hdr.ack = 1;
    send_hdr.win_size = window_size(inputs.cliwinsize);

    msgsend.msg_name = NULL;
    msgsend.msg_namelen = 0;
    msgsend.msg_iov = iovsend;
    msgsend.msg_iovlen = 1;
    iovsend[0].iov_base = (char *)&send_hdr;
    iovsend[0].iov_len = sizeof(struct hdr);
    /* Handle separately the case when last ACK is being sent */
    if ((final_seq_no > 0) && (expt_seq_no == final_seq_no+1)) {
        printf("Sending final ACk\n");
        alarm(2);
		if (drand48() < inputs.probval) {
            printf("Dropping ACK no.: %d\n", expt_seq_no);
			goto drop_packet;
		}  
        if (sendmsg(sockfd, &msgsend, 0) < 0) {
            printf("Error in sending ACK\n");
            perror("ACK: ");
        }
    } else {
		if (drand48() < inputs.probval) {
            printf("Dropping ACK no.: %d\n", expt_seq_no);
			goto drop_packet;
		}  
        if (sendmsg(sockfd, &msgsend, 0) < 0) {
            printf("Error in sending ACK\n");
            perror("ACK: ");
        }
    }

    drop_packet:
    printf("");	
    if (sigsetjmp(jmpbuf, 1) != 0) {
        printf("Last ACK received by sender, shutting down connection\n");
        close(sockfd);
        return;
    }

    }

	close(sockfd);
}
    
/* Add logic for calculating the window size, and updating it in the ack packet 
int send_ack(int sockfd, struct hdr *header) {

	struct hdr send_hdr;
	struct msghdr msgsend;
	struct iovec iovsend[1];

    printf("ACK: %d     Advertised Window Size:  %d\n", expt_seq_no, window_size());    
    memset(&msgsend, 0, sizeof(struct msghdr));
	memcpy(&send_hdr, header, sizeof(struct hdr));
	send_hdr.seq = expt_seq_no;
	send_hdr.ack = 1;
	send_hdr.win_size = window_size();

    // Final ACK is being sent, handle the time out in case the ACK is lost 
    if (send_hdr.fin == 1) {


    }
    	msgsend.msg_name = NULL;
        msgsend.msg_namelen = 0;
        msgsend.msg_iov = iovsend;
        msgsend.msg_iovlen = 1;
        iovsend[0].iov_base = (char *)&send_hdr;
        iovsend[0].iov_len = sizeof(struct hdr);	

    if (sendmsg(sockfd, &msgsend, 0) < 0) {
		printf("error in sending ack\n");
		perror("ACK:  ");
	}
}
*/
/* Reader thread will read data from the buffer and update the index, need to add mutex code */
void *reader(void *arg)
{
    struct node *temp;
    int time_val = ((struct thread_param *)arg)->time_val, sleep_value;
    struct timeval tv;
    FILE *fp = fopen("output.txt", "w");
    while (1) {
    pthread_mutex_lock(&index_mutex);
	temp = global_index.unread_buf_ptr;
	/* need to take into account the case of last packet when the data read will be less than 300 */
	char *buffer = (char *)malloc(SEGMENT_DATA_SIZE+1); 
//    fp = fopen("output.txt", "a");
	while(temp != global_index.expt_seg_ptr) {
		memcpy(buffer, temp->data, temp->seg_size);
		buffer[temp->seg_size+1] = '\0';
		printf("%s\n", buffer);
        fwrite(buffer, 1, temp->seg_size, fp);
        temp = temp->next;	

     //   printf("Moving unread_buf_ptr\n");
	}
//    fclose(fp);
	global_index.unread_buf_ptr = temp;
    pthread_mutex_unlock(&index_mutex);
    if (gettimeofday(&tv, NULL) == -1) {
        perror("");
    }
    sleep_value = (-1)*time_val*log(random_gen(tv.tv_sec + tv.tv_usec));
    sleep_value *= 1000;
    usleep(sleep_value);
    }
    fclose(fp);
}

int window_size(int size)
{
	
	struct node *temp_index = global_index.expt_seg_ptr;
	int temp_window_size = 0;

    if (temp_index == global_index.unread_buf_ptr) {
        return size;
    }

	while(temp_index != global_index.unread_buf_ptr) {
		temp_index = temp_index->next;
		temp_window_size++;
	}
	
	return temp_window_size;
}
