#include <string.h>
#include "unpifiplus.h"
#include "np1.h"

struct servsockinfo{
	in_addr_t ipaddress;
	in_addr_t netmaskaddress;
	in_addr_t subnetaddress;
 	};


struct servsockinfo servinfo[16];


int main(int argc, char **argv)
{
	int sockfd, port, len,len1,n,i,err,localflag = 0, count =0, cliwinsize, seedval, timeinmsec; 
	double probval;
	char line1[MAXLINE],line2[MAXLINE],filename[MAXLINE],sendline[MAXLINE],recvline[MAXLINE],buff[MAXLINE],*serverip;
	in_addr_t servnetmaskaddress, IPclient, longestsubnet;
	const int on = 1;
	struct sockaddr_in *sa;
	struct sockaddr *sa1;
	struct in_addr ipclient;
	struct ifi_info	*ifi, *ifihead;
	struct sockaddr_in cliaddr,servaddr,checkcliaddr,checkservaddr;
	struct clientinputs inputs;
	FILE *fp;
	
/* Read data from the file */
	fp = fopen ("client.in", "r");

	if (fp == NULL)
	{
		printf("\ncouldn't open the file client.in");
		exit(0);
	}
	
	fgets(line1, MAXLINE, fp);
	serverip = line1;
	inputs.servipaddress = inet_addr(serverip);
	fgets(line2,MAXLINE,fp);
	port = atoi(line2);
	inputs.servport= port;
	if( port < 0 )
	{
		printf("\nInvalid port number");
		exit(0);
	}

	fscanf(fp, "%s\n", filename);
	
	fgets(buff,MAXLINE,fp);
	cliwinsize = atoi(buff);
	inputs.cliwinsize = cliwinsize;
	if( cliwinsize < 0 )
	{
		printf("\nInvalid window size");
		exit(0);
	}
//    printf("sli win ini is %s",sendline);
    fgets(line2,MAXLINE,fp);
	seedval = atoi(line2);
	inputs.seedval = seedval;
	if( seedval < 0 )
	{
		printf("\nInvalid seed val");
		exit(0);
	}
	fgets(line2,MAXLINE,fp);
	probval = atof(line2);
	inputs.probval = probval;
	if( probval < 0 | probval >1)
	{
		printf("\nInvalid probabilty value");
		exit(0);
	}
	fgets(line2,MAXLINE,fp);
	timeinmsec = atoi(line2);
	inputs.timeinmsec = timeinmsec;
	if( timeinmsec < 0 )
	{
		printf("\nInvalid time");
		exit(0);
	}
	fclose(fp); 


	printf("\nclient.in file parameters:-\n");
	printf("\nIpaddress   : %s\n", serverip);
	printf("\nServer Port : %d\n", port);
	printf("\nWindow size : %d\n", cliwinsize);
	printf("\nSeed Val    : %d\n", seedval);
	printf("\nProbability : %f\n", probval);
	printf("\nTime in ms  : %d\n", timeinmsec);

	
	longestsubnet = inet_addr("0.0.0.0");
/* check if the server address is loopback address */
	if (inet_addr(serverip) == inet_addr("127.0.0.1") ){
		IPclient=inet_addr("127.0.0.1");
		localflag = 1;
	}

/* else compare with all interfaces if server address is local  */
	else{
	for (ifihead = ifi = Get_ifi_info_plus(AF_INET, 1);
		 ifi != NULL; ifi = ifi->ifi_next) 
	{
		
		if ( (sa1 = ifi->ifi_addr) != NULL)
			printf("  IP addr: %s\n",
						Sock_ntop_host(sa1, sizeof(*sa1)));
		sa = (struct sockaddr_in *) ifi->ifi_addr;
		servinfo[count].ipaddress = sa->sin_addr.s_addr;
		sa = (struct sockaddr_in *) ifi->ifi_ntmaddr;
		servinfo[count].netmaskaddress = sa->sin_addr.s_addr;

		if ( servinfo[count].ipaddress == inet_addr(serverip) ) {
			localflag = 1;
			IPclient = inet_addr("127.0.0.1");
		}

		if ( (sa1 = ifi->ifi_ntmaddr) != NULL)
			printf("  network mask: %s\n",
						Sock_ntop_host(sa1, sizeof(*sa1)));
		
		servinfo[count].subnetaddress = servinfo[count].netmaskaddress & servinfo[count].ipaddress;
		servnetmaskaddress = servinfo[count].netmaskaddress & inet_addr(serverip);
		printf("  subnetmask: %s\n", inet_ntoa(*(struct in_addr *)&servinfo[count].subnetaddress));
//		printf("  subnetmask of server: %s\n", inet_ntoa(*(struct in_addr *)&servnetmaskaddress));	
		if ( servinfo[count].subnetaddress == servnetmaskaddress && localflag != 1){
			localflag = 2;
			if (servnetmaskaddress > longestsubnet){	/* calculating the longest prefix match */
				longestsubnet = servnetmaskaddress;
				IPclient=servinfo[count].ipaddress;
			}
		}
		
		count++;
	}
	}

//	printf("\n Number of interfaces: %d\n", count);	
	count--;


	if (localflag == 1){
		printf("\nserver address is loopback address");
		printf("\nIPclient address: %s\n", inet_ntoa(*(struct in_addr *)&IPclient));}
	else if (localflag == 2){
		printf("\nserver address is local to the client");
		printf("\nIPclient address: %s\n", inet_ntoa(*(struct in_addr *)&IPclient));}
	else{
		IPclient=servinfo[count].ipaddress;	/* choosing ipaddress arbitrarily */
		printf("\nserver address is not local");
		printf("\nIPclient address: %s\n", inet_ntoa(*(struct in_addr *)&IPclient));}

/* binding the cliaddr on ephimeral port   */

	bzero(&cliaddr, sizeof(cliaddr));
	cliaddr.sin_family = AF_INET;
	cliaddr.sin_addr.s_addr= IPclient;
	cliaddr.sin_port = htons(0);
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);

	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)); 
	len = sizeof(cliaddr);

	if( (bind(sockfd, (SA *) &cliaddr, len)) == -1)
		{
			printf("\n server : Unable to bind socket : %d", sockfd);
			exit(0);
		}
/* used to retrieve client ip and port */
	len1 = sizeof(checkcliaddr);

	errno = getsockname(sockfd,(SA *)&checkcliaddr,&len1);

	printf("\nAddress and port are %s:%u",  inet_ntoa(checkcliaddr.sin_addr),  (unsigned)ntohs(checkcliaddr.sin_port));

/* connecting to a server on the server's well known port and the given ip address   */

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr= inet_addr(serverip);
	servaddr.sin_port = htons(port);

/* The Routing should not occur if ipaddress of server is local to the client */

	if (localflag == 1 | localflag == 2)
		setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR | SO_DONTROUTE , &on, sizeof(on));
	else
		setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR , &on, sizeof(on));

	len = sizeof(servaddr);

	if ( connect(sockfd, (SA *) &servaddr, len) < 0 ){
		printf("\ninitial conn");
		perror("\nconnection error");
		exit(0);
	}
	errno = getpeername(sockfd,(SA *)&checkservaddr,&len);
	printf("\nipaddress is %s",serverip);
	printf("\nServer Address and port are %s:%u",  inet_ntoa(checkservaddr.sin_addr),  (unsigned)ntohs(checkservaddr.sin_port));


//	fgets(sendline, MAXLINE, stdin);

	srand48(seedval);
sendfile:
	n = sendto(sockfd, filename, strlen(filename), 0, NULL, NULL);

	if ( n < 0 )
		printf("\nsend failed because of connection failed");
	
	n = recvfrom(sockfd, recvline, MAXLINE, 0, NULL, NULL);
	if (drand48() < probval) {
            printf(" Dropping port no\n");
			sleep(1);
			goto sendfile;
		}  
//	sprintf(sendline, "%d",cliwinsize);
/*	err = sendto(sockfd, sendline, strlen(sendline), 0, NULL, NULL);

	if ( n < 0 )
		printf("\nsend failed because of connection failed");
  */ printf("\nNew ephemeral port of server is:");
	recvline[n] = 0;
	fputs(recvline, stdout);

	servaddr.sin_family = AF_UNSPEC;
	if ( connect(sockfd, (SA *) &servaddr, len) < 0 ){
		perror("\nconnection error");
		printf("\nerror in connect - used to disconnect previous connect");
	}
	port = atoi(recvline);
	
	
	servaddr.sin_port = htons(port);
	servaddr.sin_family = AF_INET;
	if ( connect(sockfd, (SA *) &servaddr, len) < 0 ){
		printf("\nerror in reconnecting");
		perror("\nconnection error");
		exit(0);
	}
	errno = getpeername(sockfd,(SA *)&servaddr,&len);
	printf("\nServer Address and port are %s:%u",  inet_ntoa(servaddr.sin_addr),  (unsigned)ntohs(servaddr.sin_port));

	errno = getsockname(sockfd,(SA *)&checkcliaddr,&len);

	printf("\nClient Address and port are %s:%u\n\n",  inet_ntoa(checkcliaddr.sin_addr),  (unsigned)ntohs(checkcliaddr.sin_port));
printf("cli size is %s", buff);	
err = sendto(sockfd, buff, strlen(buff), 0, NULL, NULL);
//	dg_cli(stdin, sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr));

	dg_recv(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr), inputs);

close(sockfd);

return 0;
	
}



