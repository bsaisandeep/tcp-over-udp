Team Members:
--------------

1) Vinay Kumar Rai	(SBU ID # 108321304)
2) Sai Sandeep Biyyapu	(SBU ID # 108492709)

The aim of this assignment is to implement UDP socket client / server programming with a focus on two broad aspects :

(i)  Setting up the exchange between the client and server in a secure way despite the lack of a formal connection (as in TCP) between the two, so that the outsider UDP datagrams (broadcast, multicast, unicast - fortuitously or maliciously) cannot intrude on the communication.

(ii) Introducing application-layer protocol data-transmission reliability, flow control and congestion control in the client and server using TCP-like ARQ sliding window mechanisms.

Files Included:
---------------
server.c
client.c
server_helper.c
client_helper.c
np.h
np1.h
Makefile


Execution:
----------
client.in and server.in should be present in server and client folders respectively.

make
./server
./client (on same host, or local or non-local host to server)

output will be saved to output.txt

server flow: server.c and server_helper.c:
------------------------------------------
1) Binding on unicast Interfaces: Server will bind only to the unicast interfaces (resulted from Getifiinfo) and waits for the incoming connections. 

2) Select on on interfaces: Server will wait for connections using Select.

3) Read the filename and forks a child: client will send the filename to the server. Server will read the filename, checks if its duplicate request and forks of a child. (So, any request from the same cient will not be treated as a new connection).

4) Locality: Then server child checks if the the client is loopback, local or non-local based on the its own interfaces and will send its ephemeral port to the client.

Now connection between client and server is established, and server is ready to send the contents of the file to the client.

5) Time Out Mechanism: We keep a timer for the oldest unacknowledged segment on the sender side. Whenever the pointer to the oldest segment moves to next it means the oldest segment has been acknowledged and the next segment is oldest unacknowledged. At this point timer is reset and started for this segment.

6) Sliding Window: The segment buffer on the receiver and sender side is maintained as circular link list of node. The receiver side maintains two pointers: expected_buffer and last_read. expected_buffer points to the the node in which the data read from socket will be written. last_read points to the node from which the reader thread has to read data. On the sender side we maintain two pointers: last_sent and last_acknowledged. last_sent points to the node into which the file data has to be stored before sending it over socket. last_acknowledged points to the oldest unacknowledged segment.

7) Congestion Control: We are starting the congestion window with 1, and increment it by 1 for every Ack recieved back. (which means doubling for each window). Once we get any Time out or 3Dup Ack's we are reducing the congestion window to 1 and half respectively. And ssthreshold to half. 

8) Retransmission: When we recieve 3 Dup Ack's we are retransmitting the packet. (Fast Retransmission) 


9) Handling EOF: We are adding an extra flag in the header's which will say if the packet Recieved is the Last packet (End of File). Once the End of File is sent, the server sends a FIN, and waits for some time (In case a packet is lost it can resend it again), If the client sends a FIN to server, the server sends FIN_ACk and closes. Client recieves it and closes its connection too.

client flow: client.c and client_helper.c:
------------------------------------------

1) Reading parameters: Reads all the parameters from client.in

2) Checking the locality: client checks if server is loopback, local or non-local to one of its interfaces and assigns an address (using subnetmask and longest prefix match)

3) Binding to Ephemeral port: Bind to an ephemeral port number and connect to the server.

4) Send filename: Send the filename to the server.

5) Reconnect to new port: Once it recieves the server's new ephemeral port number, it disconnects the previous connection and reconnects to the new port number.

Now connection is established between the client and server, the client now will recieve the file contents and print and write into output.txt

6) Buffer and pointers: we are using a circular buffer with 2 pointer's , one pointing to the data recieved (recieve part) and other by the reader part which points to the unconsumed (yet to be printed) data.

7) Mutex Locks: We are using the Mutex lock so that the main flow and the thread will not modify the buffer pointer's at the same time. In thread we have the sleep in milliseconds ( calculated using the formula "-1 * time * log ( random() ) " so, that the client will not be executing (trying to consume) continuously. The main thread signals everytime the thread once it is done.

8) Dropping Packets: Client after recieving the data, will drop the packet's (both recieved data and Ack's to be sent) based on the probability value.

9) Handling Duplicate data: If the client encounters any duplicate data packets, it will send an Ack and ignore the packets.

10)Handling EOF: When it recieves a EOF segment (which will be known by the flag in packet header) it will send a FIN_ACk and wait and then exit's. 

np.h and np1.h:
---------------
These files contain the functions that are needed by the server_helper.c and client_helper.c and also the common functions and structures that are used by all server and client files.


